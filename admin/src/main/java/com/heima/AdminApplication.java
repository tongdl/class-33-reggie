package com.heima;

// 入口文件是放在 com/heima/admin 目录，默认的包扫描 就是 com.heima.admin
// service文件是放在 com/heima/service 目录下的，所以对应的包名是 com.heima.service

// 解决方案
// 入口文件移到 com/heima 目录下，包扫描就变成 com.heima



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class, args);
    }
}
