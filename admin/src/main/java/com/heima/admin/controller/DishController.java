package com.heima.admin.controller;

import com.heima.dto.DishDto;
import com.heima.dto.TestDto;
import com.heima.service.DishService;
import com.heima.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/dish")
public class DishController {

    // 接口三要数
    // 请求方法和地址
    // 请求入参 Request
    // 响应出参 Response
    @Autowired
    private DishService dishService;

    // 请求方法 请求地址
    @PostMapping
    // RequestBody注解后面一定要实体类
    public Result addDish(@RequestBody DishDto dishDto) {
        Boolean result = dishService.addDish(dishDto);
        if (result) {
            return new Result(200, "添加成功", null);
        }

        return new Result(500, "添加失败", null);
    }

    @PutMapping
    public Result modifyDish(@RequestBody DishDto dishDto) {
        Boolean result = dishService.modifyDish(dishDto);
        return new Result(result ? 200 : 500, result ? "修改成功" : "修改失败", null);
    }

    @DeleteMapping
    public Result removeDish(@RequestParam String ids) {

        // 我要干嘛？
        // 前端传过来的这些id，作为条件，对应删除记录
        Boolean result = dishService.removeDish(ids);

        return new Result(result ? 200 : 500, result ? "删除成功" : "删除失败", null);
    }
}
