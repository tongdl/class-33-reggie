package com.heima.service.impl;

import com.heima.mapper.OrdersMapper;
import com.heima.pojo.Orders;
import com.heima.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author itheima
 * @since 2022-12-05
 */
@Service
public class OrdersServiceImpl implements OrdersService {

    @Autowired
    private OrdersMapper ordersMapper;

    @Override
    public List<Orders> getOrdersListByToday() {
        // 开始时间 今天的0点
        // 结束时间 今天的23点59:59

        // 获得今天的日期 + 手动加上时分秒

        LocalDateTime startTime = LocalDateTime.of(2022, 12, 5, 0, 0, 0);
        LocalDateTime endTime = LocalDateTime.of(2022, 12, 5, 23, 59, 59);

        // SELECT * FROM orders WHERE order_time BETWEEN 开始时间 and 结束时间

        return ordersMapper.getOrdersListByToday(startTime, endTime);
    }
}
