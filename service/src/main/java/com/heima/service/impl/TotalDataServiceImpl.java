package com.heima.service.impl;

import com.heima.mapper.TotalDataMapper;
import com.heima.pojo.TotalData;
import com.heima.service.TotalDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author itheima
 * @since 2022-12-05
 */
@Service
public class TotalDataServiceImpl implements TotalDataService {

    @Autowired
    private TotalDataMapper totalDataMapper;

    @Override
    public Boolean addOne(TotalData totalData) {
        return totalDataMapper.addOne(totalData);
    }
}
