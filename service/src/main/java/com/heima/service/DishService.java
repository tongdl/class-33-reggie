package com.heima.service;

import com.heima.dto.DishDto;
import com.heima.pojo.Dish;
import com.heima.vo.DishVo;
import org.checkerframework.checker.units.qual.Speed;

import java.util.List;

/**
 * @author itheima
 * @since 2022-11-25
 */
public interface DishService {
    Boolean addDish(DishDto dishDto);

    Boolean modifyDish(DishDto dishDto);

    Dish getById(String dishId);

    List<DishVo> getDishList(String categoryId, Integer status, Integer page, Integer pageSize);

    List<DishVo> getDishListSimple(String categoryId, Integer status, Integer page, Integer pageSize);

    List<DishVo> getDishListPlus(String categoryId, Integer status, Integer page, Integer pageSize);

    Boolean removeDish(String ids);
}
