package com.heima.service;

import com.heima.pojo.Orders;

import java.util.List;

/**
 * @author itheima
 * @since 2022-12-05
 */
public interface OrdersService {
    List<Orders> getOrdersListByToday();
}
