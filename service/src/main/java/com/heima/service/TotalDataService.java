package com.heima.service;

import com.heima.pojo.TotalData;

/**
 * @author itheima
 * @since 2022-12-05
 */
public interface TotalDataService {
    Boolean addOne(TotalData totalData);
}
