package com.heima.mapper;

import com.heima.pojo.Dish;
import com.heima.pojo.DishFlavor;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author itheima
 * @since 2022-11-25
 */
@Mapper
@Repository
public interface DishMapper {
    Dish selectById(String dishId);

    // 结果指的是，当前sql的作用行
    int insert(@Param("dish") Dish dish);

    int insertFlavor(@Param("dishFlavor") DishFlavor dishFlavor);

    int batchInsertFlavor(@Param("list") List<DishFlavor> list);

    List<DishFlavor> selectAll();

    int updateById(@Param("dish") Dish dish);

    int countFlavorByDishId(@Param("dishId") String dishId);

    int removeFlavorByDishId(@Param("dishId") String dishId);

    List<Dish> selectByCateIdAndStatus(@Param("categoryId") String categoryId,
                                       @Param("status") Integer status,
                                       @Param("page") Integer page,
                                       @Param("pageSize") Integer pageSize);

    List<DishFlavor> selectFlavorByDishIds(@Param("dishIdList") List<String> dishIdList);

    List<DishFlavor> selectFlavorByDishId(@Param("dishId") String dishId);

    Boolean removeDishByIds(@Param("list") List<String> list);

    Boolean removeFlavorByDishIds(@Param("list") List<String> list);
}