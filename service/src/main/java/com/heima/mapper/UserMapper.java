package com.heima.mapper;

import com.heima.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author itheima
 * @since 2022-11-28
 */
@Mapper
@Repository
public interface UserMapper {
    User selectByPhone(String phone);

    int insert(@Param("user") User user);
}
