package com.heima.mapper;

import com.heima.pojo.Orders;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author itheima
 * @since 2022-12-05
 */
@Mapper
@Repository
public interface OrdersMapper {
    List<Orders> getOrdersListByToday(@Param("startTime") LocalDateTime startTime, @Param("endTIme") LocalDateTime endTIme);
}
