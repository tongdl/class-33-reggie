package com.heima.mapper;

import com.heima.pojo.Orders;
import com.heima.pojo.TotalData;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author itheima
 * @since 2022-12-05
 */
@Mapper
@Repository
public interface TotalDataMapper {
    Boolean addOne(TotalData data);
}
