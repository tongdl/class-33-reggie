package com.heima.annotation;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

// 元注解（修饰注解的注解） -> 用来描述当前注解的某些功能
// 形容当前注解在什么情况下生效（生命周期）
@Retention(value = RetentionPolicy.RUNTIME)
// 当前注解，作用范围（加载类上，加在方法上）
@Target({ElementType.METHOD})
public @interface Speed {
}
