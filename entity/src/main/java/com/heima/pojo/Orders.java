package com.heima.pojo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author itheima
 * @since 2022-12-05
 */
@Data
public class Orders {
    private String id;
    private String number;
    private Integer status;
    private String userId;
    private String addressBookId;
    private Date orderTime;
    private Date checkoutTime;
    private Integer payMethod;
    private BigDecimal amount;
    private String remark;
    private String phone;
    private String address;
    private String userName;
    private String consignee;
}
