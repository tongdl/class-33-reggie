package com.heima.pojo;

import lombok.Data;

/**
 * @author itheima
 * @since 2022-11-28
 */
@Data
public class User {
    private String id;

    private String name;

    private String phone;

    // gender
    private String sex;

    private String idNumber;

    // touxiang
    private String avatar;

    private Integer status;
}
