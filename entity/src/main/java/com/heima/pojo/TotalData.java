package com.heima.pojo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author itheima
 * @since 2022-12-05
 */
@Data
public class TotalData {
    private String id;
    private String time;
    private BigDecimal total;
}
