package com.heima.pojo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author itheima
 * @since 2022-11-25
 */
@Data
public class Dish implements Serializable {
    private String id;
    private String name;
    private String categoryId;
    private BigDecimal price;
    private String code;
    private String image;
    private String description;
    private Integer status;
    private Integer sort;
    private Date createTime;
    private Date updateTime;
    private String createUser;
    private String updateUser;
    private Integer isDeleted;
}
