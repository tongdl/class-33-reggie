package com.heima.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author itheima
 * @since 2022-11-25
 */
@Data
public class DishFlavor implements Serializable {
    private String id;
    private String dishId;
    private String name;
    private String value;
    private Date createTime;
    private Date updateTime;
    private String createUser;
    private String updateUser;
    private Integer isDeleted;
}
