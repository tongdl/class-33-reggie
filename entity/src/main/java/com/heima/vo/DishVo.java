package com.heima.vo;

import com.heima.pojo.DishFlavor;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author itheima
 * @since 2022-11-30
 */
@Data
public class DishVo implements Serializable {
    private String id;
    private String name;
    private String categoryId;
    private BigDecimal price;
    private String image;
    private String description;
    private Integer status;
    private String categoryName;

    private List<DishFlavor> flavors;
}
