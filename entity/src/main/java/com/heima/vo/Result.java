package com.heima.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author itheima
 * @since 2022-11-25
 */
@Data
// 相当于添加了一个全参数的构造方法在这里（注意一定要和空参一起用）
@AllArgsConstructor
@NoArgsConstructor
public class Result implements Serializable {
    private Integer code;
    private String msg;
    /**
     * 当接口是增删改查的时候，这个data一般是返回一个成功
     * 或者失败的信息
     *
     * 当接口是查询的时候，这个data一般返回对象
     * 可以用泛型或者用Object
     */
    private Object data;

}
