package com.heima.vo;

import lombok.Data;

/**
 * @author itheima
 * @since 2022-11-28
 */
@Data
public class LoginVo {
    private String id;
    private String phone;
    private String token;
    private Integer status;
}
