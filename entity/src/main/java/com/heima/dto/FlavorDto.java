package com.heima.dto;

import lombok.Data;

/**
 * @author itheima
 * @since 2022-11-25
 */
@Data
public class FlavorDto {
    private String name;
    private String value;
    private Boolean showOption;
}
