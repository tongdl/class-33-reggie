package com.heima.dto;

import lombok.Data;

/**
 * @author itheima
 * @since 2022-11-28
 */
@Data
public class PhoneDto {
    private String phone;
}
