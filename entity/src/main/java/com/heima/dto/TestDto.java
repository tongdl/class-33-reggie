package com.heima.dto;

import lombok.Data;

/**
 * @author itheima
 * @since 2022-11-27
 */
@Data
public class TestDto {
    private String name;
}
