package com.heima.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author itheima
 * @since 2022-11-25
 */
@Data
public class DishDto {
    private String id;
    private String name;
    private BigDecimal price;
    private String code;
    private String image;
    private String description;
    private Integer status;
    private String categoryId;

    private List<FlavorDto> flavors;
}
