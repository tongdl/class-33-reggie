package com.heima.mobile.thread;

import com.heima.pojo.User;

/**
 * @author itheima
 * @since 2022-11-28
 */
public class TokenThreadUtils {

    // 泛型指的是 ThredLocal这个容器 里面存储的数据的类型
    public static ThreadLocal<User> threadLocal = new ThreadLocal<>();

    public static User getThreadLocal() {
        return threadLocal.get();
    }

    public static void setThreadLocal(User user) {
        threadLocal.set(user);
    }

    public static void remove() {
        threadLocal.remove();
    }

}
