package com.heima.mobile.aspect;

import com.heima.annotation.Speed;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * @author itheima
 * @since 2022-12-03
 */
@Aspect
@Component
public class SpeedAspect {

    // 切入点
    // 修饰符 方法返回值类型 包名.方法名(参数列表)
    // 参数列表 .. 匹配一个或多个任意字符
    @Pointcut(value = "execution(public * com.heima.service.DishService.*(..))")
    public void pt() {

    }

    // 通知
    // com.heima.service.DishService.list()  === Object proceed = pjp.proceed();
    @Around("pt()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {

        // 统计开始时间
        long start = System.currentTimeMillis();

        // 调用方法
        Object proceed = pjp.proceed();

        // 判断当前拦截的方法，是否带有Speed注解
        // 获取当前被调用类的字节码对象（当前被执行的类的完整信息）
        Class<?> targetClass = pjp.getTarget().getClass();

        System.out.println("当前被调用的类的名字为：" + targetClass.getName());

        // 获取当前方法名
        String name = pjp.getSignature().getName();
        System.out.println("当前被执行方法的名字为" + name);

        Method[] methods = targetClass.getMethods();
        for (Method method : methods) {

            String currentMethodName = method.getName();
            if(currentMethodName.equals(name)) {
                // 判断当前方法是否带有某个注解

                boolean has = method.isAnnotationPresent(Speed.class);
                if(has) {
                    long end = System.currentTimeMillis();
                    System.out.println("接口执行毫秒数：" + (end - start));
                }

            }
        }

        return proceed;
    }

}
