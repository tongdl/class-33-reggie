package com.heima.mobile.service;

import com.heima.dto.PhoneCodeDto;
import com.heima.vo.LoginVo;

/**
 * @author itheima
 * @since 2022-11-28
 */
public interface UserService {
    Boolean sendMsg(String phone);

    LoginVo login(PhoneCodeDto dto);
}
