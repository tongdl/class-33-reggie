package com.heima.mobile.config;

import com.heima.mobile.interceptor.TokenInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * @author itheima
 * @since 2022-11-28`
 */
@Configuration
public class WebConfig extends WebMvcConfigurationSupport {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // addInterceptor 添加什么拦截器
        // addPathPatterns 过滤哪些路径
        // /** 从/开始的后面无限层级的路径 都会匹配
        // /user/info/test
        // /* 从/开始后面加上任意字符
        // /user, /orders, /test
        // /user/info 不匹配
        // excludePathPatterns 放行哪些路径
        //registry.addInterceptor(new TokenInterceptor(redisTemplate))
        //        .addPathPatterns("/**")
        //        .excludePathPatterns("/user/sendMsg", "/user/login");

    }

}
