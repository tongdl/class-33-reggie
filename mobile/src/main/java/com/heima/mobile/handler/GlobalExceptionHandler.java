package com.heima.mobile.handler;

import com.heima.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author itheima
 * @since 2022-11-28
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ResponseBody
    @ExceptionHandler(RuntimeException.class)
    public Result getRuntimeException(RuntimeException e) {
        log.info("全局异常捕获生效了");
        log.error(e.getMessage());

        return new Result(500, "运行时异常", null);
    }
}
